# Justine Smithies
# https://github.com/justinesmithies/qtile-wayland-dotfiles

# Groups configuration

from libqtile.config import Key, Group
from libqtile.command import lazy
from libqtile.config import ScratchPad, DropDown
from libqtile import qtile
if qtile.core.name == "x11":
        from keys_x11 import mod, keys, home
else:
        from keys_wayland import mod, keys, home
from workspaces import workspaces
from screens import connected_monitors
from os import environ

# Get terminal from environment variables
terminal = "xfce4-terminal"




groups = []
for workspace in workspaces:
    matches = workspace["matches"] if "matches" in workspace else None
    layouts = workspace["layout"] if "layout" in workspace else None
    groups.append(Group(workspace["name"], matches=matches, layout=layouts))
    print(workspace["key"])
    keys.append(Key([mod], workspace["key"], lazy.group[workspace["name"]].toscreen()))
    keys.append(Key([mod, "shift"], workspace["key"], lazy.window.togroup(workspace["name"])))

# for i in range(int(connected_monitors)):
#     keys.extend([Key([mod, "mod1"], str(i), lazy.window.toscreen(i))])

# Append a scratchpad group

conf = {
    "warp_pointer": False,
    "on_focus_lost_hide": True,
    "opacity": 0.80,
}



groups.append(
    ScratchPad(
        "scratchpad",
        [
            # define a drop down terminal.
            # it is placed in the upper third of screen by default.
            DropDown(
                "terminaL",
                terminal,
                height=0.90,
                width=0.999,
                x=0,
                y=0,
                **conf
            ),
        ],
    ),
)





keys.extend(
    [
        # Scratchpad
        # toggle visibiliy of above defined DropDown named "terminaL"
        # dikkat et [] kismi bos yani mod tusuna basmaya gerek yok
         Key([mod, "shift"], "Return", lazy.group["scratchpad"].dropdown_toggle("terminaL")),
    ]
)