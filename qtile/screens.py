# Justine Smithies
# https://github.com/justinesmithies/qtile-wayland-dotfiles

# Multimonitor support

from subprocess import PIPE, Popen
from libqtile.config import Screen
from libqtile import qtile
from libqtile import bar
if qtile.core.name == "x11":
    from widgets_x11 import primary_widgets, secondary_widgets
else:
    from widgets_wayland import primary_widgets, secondary_widgets
         
from colors import rancolors
#screens = [Screen(wallpaper='.cache/wallpaper', wallpaper_mode='fill', top=bar.Gap(size=38))]

#connected_monitors = 2
#if connected_monitors > 1:
#    for _ in range(1, connected_monitors):
#        screens.append(Screen(wallpaper='.cache/wallpaper', wallpaper_mode='fill', top=bar.Gap(size=38)))

def cmdline(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True,
        text=True
    )
    return process.communicate()[0]


bg = rancolors['ColorBG']
 
def status_bar(widgets):
    """Function for statusbar."""
    return bar.Bar(widgets, 30,  
                padding=20,
                opacity=0.9,
                border_width=[0, 0, 0, 0],
                margin=[0,0,0,0],
                background=bg)  # Margin = N E S W


if qtile.core.name == "x11":
      connected_monitors = cmdline("xrandr | grep ' connected '| wc -l")
else:
       connected_monitors = len(qtile.core.outputs)   

screens = [Screen(top=status_bar(primary_widgets))]

if int(connected_monitors) > 1:
        for _ in range(1, int(connected_monitors)):
            screens.append(Screen(top=status_bar(secondary_widgets)))


