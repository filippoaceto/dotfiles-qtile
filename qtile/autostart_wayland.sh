#!/bin/sh

# Autostart script for Qtile
HOMECONFIG=~/.config

export wallpaper='~/wallpaper'

dbus-update-activation-environment --systemd \
	WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=$XDG_CURRENT_DESKTOP

# Authentication dialog
pkill -f /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# Kill any existing pipewire / wireplumber daemons and only then try to start a new set.

#pkill -u "${USER}" -x pipewire\|wireplumber 1>/dev/null 2>&1
#dbus-run-session pipewire &> /dev/null &

# Start xdg-desktop-portal-wlr  

pkill -f /usr/lib/xdg-desktop-portal-wlr
/usr/lib/xdg-desktop-portal-wlr &
#/usr/lib/xdg-desktop-portal-gtk &

# Kanshi

pkill -f kanshi
kanshi &


# Notification daemon


$HOMECONFIG/qtile/scripts/wayland/notifications &

# wlsunset

pkill -f wlsunset
wlsunset -l 57.4 -L -1.9 &

nm-applet --indicator &

easyeffects --gapplication-service

$HOMECONFIG/qtile/scripts/wayland/updatewal-swww.sh


$HOMECONFIG/qtile/scripts/wayland/lockscreentime.sh


notify-send "sessione wayland"

wl-paste --watch cliphist store

lazy.to_screen(0)
# Swayidle daemon

#pkill -f swayidle
#swayidle \
#	timeout 5 'qtile cmd-obj -o core -f hide_cursor' resume 'qtile cmd-obj -o core -f unhide_cursor' \
#	timeout 300 'swaylock -f -i $wallpaper' \
#	timeout 600 'wlopm --off \*' resume 'wlopm --on \*' &
