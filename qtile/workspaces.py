# Justine Smithies
# https://github.com/justinesmithies/qtile-wayland-dotfiles

# Workspace setup
# Get the icons at https://www.nerdfonts.com/cheat-sheet

from libqtile.config import Match

workspaces = [
    {
        "name": " ₁",
        "key": "1",
        "matches": [
            Match(wm_class='firefox'),
            Match(wm_class='google-chrome'),
            Match(wm_class='Google-chrome'),
            Match(wm_class='Google-Chrome'),
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": " ₂",
        "key": "2",
        "matches": [
            Match(wm_class='foot'),
            Match(wm_class='ranger'),
            Match(wm_class='terminator'),
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": " ₃",
        "key": "3",
        "matches": [
            Match(wm_class='Eclipse'),
            Match(wm_class='code-oss')
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": " ₄",
        "key": "4",
        "matches": [
            Match(wm_class='telegram-desktop'),
            Match(wm_class='weechat'),
            Match(wm_class='teams'),
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": "󰩌 ₅",
        "key": "5",
        "matches": [
            Match(wm_class='gimp-2.99'),
            Match(wm_class='xfce4-thunar'),
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": "󰓇 ₆",
        "key": "6",
        "matches": [
            Match(wm_class='Spotify'),
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": " ₇",
        "key": "7",
        "matches": [
            Match(wm_class='soffice'),
            Match(wm_class='libreoffice-startcenter'),
            Match(wm_class='libreoffice-writer'),
            Match(wm_class='libreoffice-base'),
            Match(wm_class='libreoffice-calc'),
            Match(wm_class='libreoffice-draw'),
            Match(wm_class='libreoffice-impress'),
            Match(wm_class='libreoffice-math'),
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": "󰎕 ₈",
        "key": "8",
        "matches": [
            Match(wm_class='newsboat'),
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": "󰇮 ₉",
        "key": "9",
        "matches": [
            Match(wm_class='aerc'),
        ],
        "layout": "monadtall",
        "spawn": [],
    },
    
#    {
#        "name": " ₁₀",
#        "key": "10",
#        "matches": [
#        
#        ],
#        "layout": "monadtall",
#        "spawn": [],
#    },

]
