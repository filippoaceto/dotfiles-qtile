#!/bin/sh

# Autostart script for Qtile

HOMECONFIG=~/.config/qtile

export wallpaper='~/wallpaper'




# Load power manager
xfce4-power-manager &
#xfce4-screensaver &
xscreensaver -no-splash &

easyeffects --gapplication-service


# Notification daemon

# Start xdg-desktop-portal-wlr  

pkill -f /usr/lib/xdg-desktop-portal-wlr
/usr/lib/xdg-desktop-portal-wlr &


nm-applet --indicator &


# Authentication dialog
pkill -f /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &



$HOMECONFIG/scripts/x11/notifications &


$HOMECONFIG/scripts/x11/dual.sh &

# Setup Wallpaper and update colors
$HOMECONFIG/scripts/x11/updatewal.sh &


# Load picom
picom &


