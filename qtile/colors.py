import os
import json
#Pywal Colors
colors = os.path.expanduser('~/.cache/wal/colors.json')
colordict = json.load(open(colors))

rancolors = {
        'ColorBG':(colordict['special']['background']),
        'ColorFG':(colordict['special']['foreground']),
        'ColorCU':(colordict['special']['cursor']),
        'ColorZ':(colordict['colors']['color0']),
        'ColorA':(colordict['colors']['color1']),
        'ColorB':(colordict['colors']['color2']),
        'ColorC':(colordict['colors']['color3']),
        'ColorD':(colordict['colors']['color4']),
        'ColorE':(colordict['colors']['color5']),
        'ColorF':(colordict['colors']['color6']),
        'ColorG':(colordict['colors']['color7']),
        'ColorH':(colordict['colors']['color8']),
        'ColorI':(colordict['colors']['color9']),
        'ColorL':(colordict['colors']['color10']),
        'ColorM':(colordict['colors']['color11']),
        'ColorN':(colordict['colors']['color12']),
        'ColorO':(colordict['colors']['color13']),
        'ColorP':(colordict['colors']['color14']),
        'ColorQ':(colordict['colors']['color15'])
}

