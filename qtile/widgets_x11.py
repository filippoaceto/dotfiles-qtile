# Justine Smithies
# https://github.com/justinesmithies/qtile-wayland-dotfiles

# Widgets setup
# Get the icons at https://www.nerdfonts.com/cheat-sheet

import os
import subprocess
from subprocess import PIPE, Popen
from libqtile import qtile
from qtile_extras.widget.decorations import RectDecoration
from qtile_extras.widget.decorations import PowerLineDecoration
from qtile_extras import widget
from libqtile import widget as old_widget
from colors import colors
from colors import rancolors
from ordinaldate import custom_date
from keys_wayland import terminal
from libqtile.command import lazy


def cmdline(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True,
        text=True
    )
    return process.communicate()[0]

@lazy.function
def update_arch():
     qtile.spawn(os.path.expanduser(home +"qtile/scripts/volume --get-pavucontrol"))



home = os.path.expanduser('~/.config/')


widget_defaults = dict(
    font='GoMono Nerd Font',
    fontsize='12',
    padding=2,
    foreground=rancolors['ColorB']
)
extension_defaults = widget_defaults.copy()

# --------------------------------------------------------
# Decorations
# https://qtile-extras.readthedocs.io/en/stable/manual/how_to/decorations.html
# --------------------------------------------------------

decor_left = {
    "decorations": [
        PowerLineDecoration(
            path="arrow_left"
            # path="rounded_left"
            # path="forward_slash"
            # path="back_slash"
        )
    ],
}

decor_right = {
    "decorations": [
        PowerLineDecoration(
            path="arrow_right"
            # path="rounded_right"
            # path="forward_slash"
            # path="back_slash"
        )
    ],
}

decor_groupbox = {
"decorations": [
    RectDecoration(colour=rancolors['ColorBG'], radius=10,
                   filled=True, padding_y=2, padding_x=2)
],
"padding": 0
}

decor_systray = {
"decorations": [
    RectDecoration(colour=rancolors['ColorD'], radius=10,
                   filled=True, padding_y=0, padding_x=0)
],
"padding": 0
}


decor_groupbox2 = {
"decorations": [
    RectDecoration(colour=rancolors['ColorF'], radius=70,
                   filled=True, padding_y=2, padding_x=3)
],
"padding": 0
}


connected_monitors = cmdline("xrandr | grep ' connected '| wc -l")

if int(connected_monitors) > 1:
        decor_primary_widget = dict(
        visible_groups=[' ₁',' ₂', ' ₃', ' ₄', '󰩌 ₅']
    )
else:
        decor_primary_widget = dict(
        visible_groups=[' ₁',' ₂', ' ₃', ' ₄', '󰩌 ₅','󰓇 ₆',' ₇','󰎕 ₈','󰇮 ₉']
    )


decor_secondary_widget = dict(
        visible_groups=['󰓇 ₆',' ₇','󰎕 ₈','󰇮 ₉']
)


    

primary_widgets = [


    widget.Image(**decor_left,background='ffffff.5',mouse_callbacks={'Button1': lambda: qtile.spawn(os.path.expanduser(home + "qtile/scripts/applauncher.sh"))},
                  filename= home +'qtile/images/arch.svg', **widget_defaults),         
    #widget.Spacer(length=10),   
    widget.GroupBox(
        **decor_left,
        #**decor_primary_widget,
        background=rancolors['ColorBG']+".10",
        active=rancolors['ColorFG'],
        inactive=rancolors['ColorB'],
        highlight=rancolors['ColorE'],
        borderwidth=3,
        #this_current_screen_border='2098ae',
        #this_screen_border='2098ae',
        this_current_screen_border=rancolors['ColorA'],
        this_screen_border=rancolors['ColorC'],
        other_screen_border=rancolors['ColorO'],
        other_current_screen_border=rancolors['ColorO'] + ".5",
        font='GoMono Nerd Font',
        fontsize=18,
        foreground='ffffff',
        highlight_method='block',
        highlight_color=[rancolors['ColorE'], rancolors['ColorA']],
    ),


    widget.CurrentLayoutIcon(scale=0.5, **decor_left,background="#ffffff.4",foreground="000000.6",fontsize=18),
    widget.CurrentLayout(scale=0.5, **decor_left,background="#ffffff.4",foreground="000000.6",fontsize=15),
    #widget.Spacer(length=10),
    widget.TextBox(scale=0.5, **decor_left,text=" ",background="#ffffff.4",foreground="000000.6",fontsize=15,mouse_callbacks={"Button1": lambda: qtile.cmd_spawn("google-chrome-stable")}),
    widget.TextBox(scale=0.5, **decor_left,text=" ",background="#ffffff.4",foreground="000000.6",fontsize=15,mouse_callbacks={"Button1": lambda: qtile.cmd_spawn("thunar")}),
    
    # widget.Spacer(),
    #  widget.WindowName(
    #     **decor_left,
    #     max_chars=50,
    #     background=rancolors['ColorBG'],
    #     width=300,
    #     padding=10
    # ),

 widget.TaskList(
                     **decor_left,
                    fontsize=12, 
                    padding=4, 
                    margin=3,
                    borderwidth=2,
                    linewidth = 10,
                    background=rancolors['ColorBG']+".3",
                    foreground=rancolors['ColorFG']+".7",
                    border = 'ffffff',
                    urgent_alert_method = 'border',
                    urgent_border = rancolors['ColorG'],
                    #unfocused_border = rancolors['ColorH'],
                    title_width_method = 'uniform',
                    highlight_method = 'line' ,
                    max_title_width = 200,
                    #fmt = 'center',
                    theme_mode = 'fallback',
                    theme_path = '/usr/share/icons/Papirus-Dark/',
                    icon_size = 18,
                    txt_floating = '🗗  ', 
                    txt_maximized = '🗖  ',
                    txt_minimized = '🗕  ',
                    markup_focused = '<span color="' + rancolors['ColorH'] + '">{}</span>',
                    markup_maximized = '<span color="' + rancolors['ColorC'] + '">{}</span>',
                    markup_floating = '<span color="' + rancolors['ColorC'] + '">{}</span>',
                    markup_minimized = '<span color="' + rancolors['ColorC'] + '">{}</span>',
                    markup_normal = '<span color="' + rancolors['ColorC'] + '">{}</span>',
                    rounded='True'

 ),

    # widget.Spacer(),

    widget.CheckUpdates(
     
        fontsize=15,
        distro='Arch_yay',
        background='ffffff.8',
        colour_have_updates=rancolors['ColorBG'],
        colour_no_updates=rancolors['ColorBG'],
        no_update_string='󰚰 0',
        display_format='󰚰 {updates}',
        update_interval=1800,
        execute='terminator -e "yay -Syu"',
    ),






      widget.Memory(
        **decor_right,
        fontsize = 15,
        background=rancolors['ColorBG'],
        foreground=rancolors['ColorFG'],
        padding=10,        
        measure_mem='G',
        format="󰍛 {MemUsed:.0f}{mm} ({MemTotal:.0f}{mm})"
    ),
      widget.DF(
        **decor_right,
        padding=10, 
        fontsize = 15,
        background='ffffff.8',
        visible_on_warn=False,
        format="󰋊 {p} {uf}{m} ({r:.0f}%)"
    ),


    widget.Systray(
        **decor_right,
        background=rancolors['ColorC'],
        icon_size = 20
    ),
   # widget.Spacer(length=5),
    widget.KeyboardLayout(
        **decor_right,
         background='ffffff.8',
         configured_keyboards=['us intl', 'it'],
    ),
    #widget.Spacer(length=5),
    widget.GenPollText(**decor_right,update_interval=1, 
                       background='ffffff',
                       func=lambda: subprocess.check_output(os.path.expanduser(home +"qtile/statusbar/brightnesscontrol")).decode(),
                       mouse_callbacks={
                                        'Button5': lambda: qtile.spawn(os.path.expanduser("~/.config/qtile/statusbar/brightnesscontrol down"), shell=True), 
                                        'Button4': lambda: qtile.spawn(os.path.expanduser("~/.config/qtile/statusbar/brightnesscontrol up"), shell=True)
                                      }
                        ),

   # widget.Spacer(length=5),
    widget.GenPollText(**decor_right,update_interval=1, background='ffffff.8',
                       scroll=True,                
                       func=lambda: subprocess.check_output(os.path.expanduser(home +"qtile/scripts/volume")).decode(), 
                       mouse_callbacks={
                                         'Button5': lambda: qtile.spawn(os.path.expanduser(home +"qtile/scripts/volume --dec"), shell=True), 
                                         'Button1': lambda: qtile.spawn(os.path.expanduser(home +"qtile/scripts/volume --get-pavucontrol"), shell=True), 
                                         'Button4': lambda: qtile.spawn(os.path.expanduser(home +"qtile/scripts/volume --inc"), shell=True)
                                        }
                      ),
    #widget.Spacer(length=5),
    #widget.GenPollText(background='ffffff.8',update_interval=1, func=lambda: subprocess.check_output(os.path.expanduser(home +"qtile/statusbar/battery.py")).decode(), mouse_callbacks={'Button1': lambda: qtile.spawn(os.path.expanduser(home +"/qtile/statusbar/battery.py --c left-click"), shell=True)}),
    #widget.Spacer(length=5),
    #widget.GenPollText(background='ffffff.',update_interval=1, func=lambda: subprocess.check_output(os.path.expanduser(home +"qtile/statusbar/network.sh")).decode(), mouse_callbacks={'Button1': lambda: qtile.spawn(os.path.expanduser(home +"/qtile/statusbar/network.sh ShowInfo"), shell=True), 'Button3': lambda: qtile.spawn(terminal + ' -e nmtui', shell=True)}),
    #widget.Spacer(length=10),
    widget.Clock(
        **decor_right,
        background='ffffff',
        padding=10,      
        format="%Y-%m-%d / %I:%M %p",
    ),
    widget.TextBox(
        background=rancolors['ColorE'],
        padding=5,    
        text="⏻ ",
        fontsize=20,
        mouse_callbacks={"Button1": lambda: qtile.cmd_spawn(home + "qtile/scripts/x11/powermenu")},
    ),

          widget.Prompt(
                       prompt = 'prompt',
                       font = "Ubuntu Mono",
                       padding = 10,
                       foreground = rancolors['ColorFG'],
                       background = rancolors['ColorBG']
                       ),

]

secondary_widgets = [
   widget.GroupBox(
       **decor_left,
       #**decor_secondary_widget,
        background=rancolors['ColorBG']+".10",
        active=rancolors['ColorFG'],
        inactive=rancolors['ColorB'],
        highlight=rancolors['ColorA'],
        borderwidth=3,
        #this_current_screen_border='2098ae',
        #this_screen_border='2098ae',
        this_current_screen_border=rancolors['ColorA'],
        this_screen_border=rancolors['ColorC'],
        other_screen_border=rancolors['ColorO'],
        other_current_screen_border=rancolors['ColorO'] + ".5",
        font='GoMono Nerd Font',
        fontsize=18,
        foreground='ffffff',
        highlight_method='block',
        highlight_color=[rancolors['ColorE'], rancolors['ColorA']]
    ),
    widget.CurrentLayoutIcon(scale=0.5, **decor_left,background="#ffffff.4",foreground="000000.6",fontsize=18),
    widget.CurrentLayout(scale=0.5, **decor_left,background="#ffffff.4",foreground="000000.6",fontsize=15),
    #widget.Spacer(length=10),
    widget.TextBox(scale=0.5, **decor_left,text=" ",background="#ffffff.4",foreground="000000.6",fontsize=15,mouse_callbacks={"Button1": lambda: qtile.cmd_spawn("google-chrome-stable")}),
    widget.TextBox(scale=0.5, **decor_left,text=" ",background="#ffffff.4",foreground="000000.6",fontsize=15,mouse_callbacks={"Button1": lambda: qtile.cmd_spawn("thunar")}),
    

    old_widget.TaskList(
                    fontsize=12, 
                    padding=4, 
                    margin=3,
                    borderwidth=2,
                    linewidth = 10,
                    background=rancolors['ColorBG']+".9",
                    foreground=rancolors['ColorFG']+".2",
                    border = 'ffffff',
                    urgent_alert_method = 'border',
                    urgent_border = rancolors['ColorG'],
                    #unfocused_border = rancolors['ColorH'],
                    title_width_method = 'uniform',
                    highlight_method = 'line' ,
                    max_title_width = 200,
                    #fmt = 'center',
                    theme_mode = 'fallback',
                    theme_path = '/usr/share/icons/Papirus-Dark/',
                    icon_size = 18,
                    txt_floating = '🗗  ', 
                    txt_maximized = '🗖  ',
                    txt_minimized = '🗕  ',
                    markup_focused = '<span color="' + rancolors['ColorA'] + '">{}</span>',
                    markup_maximized = '<span color="' + rancolors['ColorC'] + '">{}</span>',
                    markup_floating = '<span color="' + rancolors['ColorC'] + '">{}</span>',
                    markup_minimized = '<span color="' + rancolors['ColorC'] + '">{}</span>',
                    markup_normal = '<span color="' + rancolors['ColorC'] + '">{}</span>',
                    rounded='True'
 ),

]

