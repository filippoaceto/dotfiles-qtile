#/bin/sh
#source /home/filippo/dotfiles/scripts/lockscreen	


if [ -f "/usr/bin/swayidle" ]; then
    echo "swayidle is installed."
    swayidle -w \
	timeout 5 'qtile cmd-obj -o core -f hide_cursor' resume 'qtile cmd-obj -o core -f unhide_cursor' \
        timeout 300 ~/.config/qtile/scripts/wayland/lockscreen  \
        timeout 600 'wlopm --off \*' resume 'wlopm --on \*' &
else
    echo "swayidle not installed."
fi;
