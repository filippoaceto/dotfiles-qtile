#!/bin/bash
#  ____                               _           _    
# / ___|  ___ _ __ ___  ___ _ __  ___| |__   ___ | |_  
# \___ \ / __| '__/ _ \/ _ \ '_ \/ __| '_ \ / _ \| __| 
#  ___) | (__| | |  __/  __/ | | \__ \ | | | (_) | |_  
# |____/ \___|_|  \___|\___|_| |_|___/_| |_|\___/ \__| 
#                                                      
#  
# by Stephan Raabe (2023) 
# ----------------------------------------------------- 

DIR="/home/filippo/Immagini/Schermate/"
NAME="screenshot_$(date +%d%m%Y_%H%M%S).png"

# option1="Selected window (delay 3 sec)"
option2="Selected area"
option3="Fullscreen (delay 3 sec)"
option4="Fullscreen Schermo Principale (delay 3 sec)"
option5="Fullscreen Schermo Secondario HDMI (delay 3 sec)"
option6="Copy Selected area"



# options="$option1\n$option2\n$option3"
options="$option2\n$option3\n$option4\n$option5\n$option6"

choice=$(echo -e "$options" | rofi -dmenu -i -no-show-icons -l 5 -width 40 -p "Take Screenshot")

case $choice in
    $option1)
        scrot | wl-copy $DIR$NAME -d 3 -e 'xclip -selection clipboard -t image/png -i $f' -c -z -u
        notify-send "Screenshot created" "Mode: Selected window"
    ;;
    $option2)
        grim | wl-copy -g "$(slurp)" $(xdg-user-dir)/Immagini/Schermate/$(date +'%s_grim.png')
        notify-send "Screenshot created" "Mode: Selected area"
    ;;
    $option3)
        sleep 3
        grim $(xdg-user-dir)/Immagini/Schermate/$(date +'%s_grim.png')
        notify-send "Screenshot created" "Mode: Fullscreen"
    ;;
    $option4)
        sleep 3
        grim -o eDP-1 $(xdg-user-dir)/Immagini/Schermate/$(date +'%s_grim.png')
        notify-send "Screenshot created" "Mode: Fullscreen"
    ;;
    $option5)
        sleep 3
        grim -o HDMI-A-1 $(xdg-user-dir)/Immagini/Schermate/$(date +'%s_grim.png')
        notify-send "Screenshot created" "Mode: Fullscreen"
    ;;
    $option6)
        grim -g "$(slurp)" -t png - | wl-copy -t image/png
        notify-send "Screenshot created" "Mode: Fullscreen"
    ;;

esac
