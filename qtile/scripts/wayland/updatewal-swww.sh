#!/bin/bash
#  _____ _                           ______        ____        ____        __ 
# |_   _| |__   ___ _ __ ___   ___  / ___\ \      / /\ \      / /\ \      / / 
#   | | | '_ \ / _ \ '_ ` _ \ / _ \ \___ \\ \ /\ / /  \ \ /\ / /  \ \ /\ / /  
#   | | | | | |  __/ | | | | |  __/  ___) |\ V  V /    \ V  V /    \ V  V /   
#   |_| |_| |_|\___|_| |_| |_|\___| |____/  \_/\_/      \_/\_/      \_/\_/    
#                                                                             
#  
# by Stephan Raabe (2023) 
# ----------------------------------------------------- 


HOMECONFIG=~/.config/

# ----------------------------------------------------- 
# Select random wallpaper and create color scheme
# ----------------------------------------------------- 
swww query || swww init


# ----------------------------------------------------- 
# Load current pywal color scheme
# ----------------------------------------------------- 
source "$HOME/.cache/wal/colors.sh"

# ----------------------------------------------------- 
# Copy color file to waybar folder
# ----------------------------------------------------- 
#cp ~/.cache/wal/colors-waybar.css ~/configurazioni-hyprland/dotfiles/waybar/
wallpaper=$(find ~/wallpaper/ -type f | shuf --random-source=/dev/urandom -n 1)
#wallpaper2=$(find ~/wallpaper/ -type f | shuf --random-source=/dev/urandom -n 1)

cp $wallpaper ~/.cache/current_wallpaper.jpg 



wal -q -i $wallpaper

cp ~/.cache/wal/colors-mako.conf $HOMECONFIG/qtile/mako/config.conf



# ----------------------------------------------------- 
# Set the new wallpaper
# ----------------------------------------------------- 
swww img -o eDP-1,DP-1 $wallpaper \
    --transition-bezier .43,1.19,1,.4 \
    --transition-fps=60 \
    --transition-type="random" \
    --transition-duration=0.7 \
#    --transition-pos "$( hyprctl cursorpos )" 

#swww img -o DP-1 $wallpaper2 \
#    --transition-bezier .43,1.19,1,.4 \
#    --transition-fps=60 \
#    --transition-type="random" \
#    --transition-duration=0.7 \
##    --transition-pos "$( hyprctl cursorpos )"

  

$HOMECONFIG/qtile/scripts//wayland/gtk.sh
$HOMECONFIG/qtile/scripts/wayland/notifications &
sleep 1


# ----------------------------------------------------- 
# Send notification
# ----------------------------------------------------- 
notify-send "Theme and Wallpaper updated" "With image $wallpaper"

echo "DONE!"
