#!/bin/bash
clear
source .install/library.sh
source .install/qtile.sh
source .install/confirm-start.sh
source .install/yay.sh
source .install/qtile-packages.sh
source .install/install-packages.sh
source .install/pywal.sh
source .install/sddm.sh
source .install/config-folder.sh
source .install/qtile-dotfiles.sh
source .install/init-pywal.sh

echo ""
source .install/done.sh