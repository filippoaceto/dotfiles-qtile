# ------------------------------------------------------
# Enable sddm display manager
# ------------------------------------------------------

echo "-> Enable sddm service and setup theme"
while true; do
    read -p "Do you want to enable the sddm display manager and setup theme? (Yy/Nn): " yn
    case $yn in
        [Yy]* )
            if [ -f /etc/systemd/system/display-manager.service ]; then
                sudo rm /etc/systemd/system/display-manager.service
                echo "Current display manager removed."
            fi

            sudo systemctl enable sddm.service
            sudo systemctl start sddm.service
            echo "sddm installed and started."

            if [ ! -d /etc/sddm.conf.d/ ]; then
                sudo mkdir /etc/sddm.conf.d
                echo "Folder /etc/sddm.conf.d created."
            fi

            sudo cp ~/dotfiles/sddm/sddm.conf /etc/sddm.conf.d/
            echo "File /etc/sddm.conf.d/sddm.conf updated."

            sudo cp ~/.cache/current_wallpaper.jpg /usr/share/sddm/themes/sugar-candy/Backgrounds/
            echo "Current wallpaper copied into /usr/share/sddm/themes/sugar-candy/Backgrounds/"

            sudo cp ~/dotfiles/sddm/theme.conf /usr/share/sddm/themes/sugar-candy/
            echo "File theme.conf updated in /usr/share/sddm/themes/sugar-candy/"

        break;;
        [Nn]* ) 
            echo "sddm installation skipped."
        break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ""
