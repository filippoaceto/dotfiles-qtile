# ------------------------------------------------------
# Install dotfiles
# ------------------------------------------------------

echo "-> Install dotfiles"
echo "The script will now remove existing directories and files from ~/.config/"
echo "Symbolic links will then be created from ~/dotfiles-qtile into your ~/.config/ directory."
echo "PLEASE BACKUP YOUR EXISTING CONFIGURATIONS IF NEEDED."
echo ""

while true; do
    read -p "Do you want to install the dotfiles now? (Yy/Nn): " yn
    case $yn in
        [Yy]* )
            _installSymLink starship ~/.config/starship.toml ~/dotfiles-qtile/starship/starship.toml ~/.config/starship.toml
            _installSymLink rofi ~/.config/rofi ~/dotfiles-qtile/rofi/ ~/.config
            _installSymLink dunst ~/.config/dunst ~/dotfiles-qtile/dunst/ ~/.config
            _installSymLink wal ~/.config/wal ~/dotfiles-qtile/wal/ ~/.config

            wal -i ~/dotfiles-qtile/wallpapers/default.jpg
            echo "Pywal and templates initiated!"
            echo ""

            _installSymLink qtile ~/.config/qtile ~/dotfiles-qtile/qtile/ ~/.config
            _installSymLink picom ~/.config/picom ~/dotfiles-qtile/picom/ ~/.config
            _installSymLink .xinitrc ~/.xinitrc ~/dotfiles-qtile/qtile/.xinitrc ~/.xinitrc
            _installSymLink swaylock ~/.config/swaylock ~/dotfiles-qtile/swaylock/ ~/.config
            _installSymLink .gtkrc-2.0 ~/.gtkrc-2.0 ~/dotfiles-qtile/gtk/.gtkrc-2.0 ~/.gtkrc-2.0
            _installSymLink .Xresources ~/.Xresources ~/dotfiles-qtile/gtk/.Xresources ~/.Xresources
            _installSymLink gtk-3.0 ~/.config/gtk-3.0 ~/dotfiles-qtile/gtk/gtk-3.0/ ~/.config/ 
            _installSymLink kanshi ~/.config/kanshi ~/dotfiles-qtile/kanshi ~/.config      
            _installSymLink fuzzel ~/.config/fuzzel ~/dotfiles-qtile/fuzzel ~/.config      

        break;;
        [Nn]* ) 
            echo "Installation of dotfiles skipped."
        break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ""