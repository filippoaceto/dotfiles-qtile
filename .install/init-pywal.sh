# ------------------------------------------------------
# init pywal with default wallpaper
# ------------------------------------------------------

_installSymLink wal ~/.config/wal ~/dotfiles-qtile/wal/ ~/.config
wal -i ~/dotfiles-qtile/wallpapers/default.jpg
echo "Pywal and templates initiated!"
echo ""
